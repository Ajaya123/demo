import "reflect-metadata";
import {createConnection} from "typeorm";
import {User} from "./entity/User";
import{Photo} from "./entity/Photo";

createConnection().then(async connection => {

    console.log("Inserting a new user into the database...");
    const user = new User();
    user.firstName = "Timber";
    user.lastName = "Saw";
    user.age = 25;
    await connection.manager.save(user);
    console.log("Saved a new user with id: " + user.id);

    console.log("Loading users from the database...");
    const users = await connection.manager.find(User);
    console.log("Loaded users: ", users);
}).catch(error => console.log(error));
createConnection().then(async connection=>{
    console.log("inserting data in photo");
    const photo=new Photo();
    photo.name="ajaya";
    photo.description="its my image";
    photo.fileName="ajaya.jpg";
    photo.views=2;
    photo.isPublished=true;
    await connection.manager.save(photo);
    console.log("save a new photo :"+photo.id);
    let savephoto=await connection.manager.find(Photo);
    console.log("see all photos :",savephoto);
}).catch(error=>console.log(error));

